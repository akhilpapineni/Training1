package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

/**
 * Servlet implementation class CustomerInfo
 */
@WebServlet("/CustomerInfo")
public class CustomerInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	
	
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	int id= Integer.parseInt(request.getParameter("customerId"));
    	String name= request.getParameter("customerName");
    	String address=request.getParameter("customerAddress");
    	int billAmount= Integer.parseInt(request.getParameter("billAmount"));
    	
    	Customer customer= new Customer(id, name, address, billAmount );
        CustomerDAO customerDAO= new CustomerDAOImpl();
        customerDAO.insertCustomer(customer);
        
        
    	response.setContentType("text/html");
    	response.getWriter().println("<h2>Customer Id: </h2>" +id);
    	response.getWriter().println("<h2>Customer Name: </h2>" +name);
    	response.getWriter().println("<h2>Customer Address: </h2>" +address);
    	response.getWriter().println("<h2>Customer BillAmount: </h2>" +billAmount);
    	
    	
    	
    	
    	
    	
    	
    	
    	
    }
    
}