package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.parser.Cookie;

/**
 * Servlet implementation class Welcome
 */
@WebServlet("/Welcome")
public class Welcome extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Welcome() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
    int counter=0;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		counter++;
		String username=request.getParameter("uname");
		String password=request.getParameter("upass");
		
		boolean alreadyVisited=false;
		javax.servlet.http.Cookie[] allCookies=request.getCookies();
		
		if(allCookies!=null)
		{
			for(javax.servlet.http.Cookie c: allCookies)
			{
				if(c.getName().equals(username) && c.getValue().equals(password))
				{
					alreadyVisited=true;
					break;
				}
			}
		}
		if(alreadyVisited)
		{
			response.getWriter().println("<h1>You already visited -"+username +"</h1>");
			response.getWriter().println("<br/><a href= enter.html>Enter</a>");
		}
		else
		{
			response.getWriter().println("<h1>You are first time visitor -"+username +"</h1>");
			response.getWriter().println("<br/><a href=enter.html>enter</a>");
			javax.servlet.http.Cookie ca= new javax.servlet.http.Cookie(username, password);
			response.addCookie(ca);
			
   		}
		
		
//		if(username.equalsIgnoreCase("akhil"))
//		{
//		HttpSession session= request.getSession();
//		session.setAttribute("nn", username);
//		
//		//response.sendRedirect("enter.html");
//		
//		request.getRequestDispatcher("Enter");
//		response.getWriter().println("<h1>Welcome to our website- "+username+"</h1>");
//		response.getWriter().println("<font color =green><h1>You are visitor number: </h1>"+counter );
//		response.getWriter().println("<br/><a href=enter.html>enter</a>");
//		}
//		else
//		{
//			response.sendRedirect("home.html");
//		}
		
	}

}
