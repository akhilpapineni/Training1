package com.javatraining.dbcon;

import java.sql.*;
import java.util.Scanner;

public class InsertDemo {

	public static void main(String[] args) throws SQLException {
		Scanner sc=new Scanner(System.in);
		
		
		System.out.println("Enter customer ID");
		int customerId=sc.nextInt();
		
		System.out.println("Enter customer name");
		String customerName=sc.next();
		
		
		System.out.println("Enter customer address");
		String customerAddress=sc.next();
		
		System.out.println("Enter customer billamount");
		int billAmount=sc.nextInt();
		
		
		
		Connection connection= DBConfig.getConnection();
		PreparedStatement statement=connection.prepareStatement("insert into customer values(?,?,?,?)");
		statement.setInt(1, customerId);
		statement.setString(2, customerName);
		statement.setString(3, customerAddress);
		statement.setInt(4, billAmount);
		
		int rows= statement.executeUpdate();
		System.out.println(rows +"Data inserted");
		
		

	}

}
