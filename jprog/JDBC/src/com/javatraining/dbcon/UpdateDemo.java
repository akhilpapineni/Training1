package com.javatraining.dbcon;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class UpdateDemo {
	
	public static void main(String[] args) throws SQLException {
		Scanner sc=new Scanner(System.in);
		
		
		System.out.println("Enter customer ID to update");
		int customerId=sc.nextInt();
		
		System.out.println("Enter customer name");
		String customerName=sc.next();
		
		
		System.out.println("Enter customer address");
		String customerAddress=sc.next();
		
		System.out.println("Enter customer billamount");
		int billAmount=sc.nextInt();
		
		Connection connection= DBConfig.getConnection();

		  String query = "update customer set customerAddress = ? , billAmount=? where customerId = ?";
	      PreparedStatement preparedStatement = connection.prepareStatement(query);
	      preparedStatement.setString(1,customerAddress );
	      preparedStatement.setInt(2, billAmount);

		
		
		int rows=preparedStatement.executeUpdate();
		System.out.println("customerId with " +customerId + " updated with newaddress" +customerAddress+ " and new bill is" +billAmount);
		System.out.println(rows + " Updated");
	      
	      connection.close();
		
		

	}

}


