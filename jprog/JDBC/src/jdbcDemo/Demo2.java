package jdbcDemo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.javatraining.dbcon.DBConfig;

public class Demo2 {

	public static void main(String[] args) throws SQLException {
		Scanner sc= new Scanner(System.in);
		
		System.out.println("Enter user id ");
		String UserId=sc.next();
		 
		System.out.println("Enter password ");
		String Password=sc.next();
		
		Connection connection= DBConfig.getConnection();
		PreparedStatement statement= connection.prepareStatement("select * from UserDetails where UserId = ? and Password = ?");
		statement.setString(1, UserId);
		statement.setString(2, Password);
		
		ResultSet res = statement.executeQuery(); 
		if(res.next())
		{
			System.out.println("Logging in");
		}
		else
		{
			System.out.println("wrong credentials");
		
		}
		
		 
	}

}
