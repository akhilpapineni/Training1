<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cricworld</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>


	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="home.html"><span>CRIC</span>world</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1  " href="home.html">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming
								Matches</a></li>
						<li class="dropdown"><a class="pad_1 active_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="legendary_moments.jsp">Legendary
								Moments</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>
					</li>
					</ul>
					</li>

					</ul>
					<ul class="nav navbar-nav navbar-right nav_2">



						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>

	<section id="center">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="pages">
					<div class="container" role="main">


						<h2>
							IRELAND</a>
						</h2>
						<br> <img src="img/ireland.jpg" width="100%"></a>
						<p>The Ireland cricket team represents all of Ireland. They
							participate in Test, ODI and Twenty20 matches at international
							level. They are the 11th Full Member of the International Cricket
							Council (ICC), having been awarded Test status, along with
							Afghanistan, on 22 June 2017.</p>
						<p>Ireland are ranked 12th in One Day International (ODI)
							cricket. Ireland played their first ODI in 2006 against England.
							Since then, they have gone on to play 117 ODIs, resulting in 51
							victories, 57 defeats, 7 no results, and 3 ties.Contracts for
							players were introduced in 2009, marking the transition to
							becoming a professional team. Cricket Ireland is the sport's
							governing body in Ireland.</p>
						<p>Cricket was introduced to Ireland in the 19th century, and
							the first match played by an Ireland team was in 1855. Ireland
							toured Canada and the United States in the late 19th century, and
							occasionally hosted matches against touring sides. Rivalry with
							the Scotland national cricket team was established when the teams
							first played each other in 1888. Ireland's maiden first-class
							match was played in 1902.</p>
						<p>In 1993 the Irish Cricket Union, the predecessor to Cricket
							Ireland, was elected to the ICC as an Associate member.
							Associates are the next level of team below those that play Test
							cricket. Due to their successes in the Intercontinental Cup and
							at the World Cup, they were labelled the "leading Associate" and
							stated their intention to become a full member by 2020. This
							intention was realised in June 2017, when the ICC unanimously
							decided to award Ireland and Afghanistan full Test status, which
							allows them to participate in Test matches.</p>
						<br>
						<h2>See Also</h2>
						<form action="players.html">
							<div class="col-sm-6">
								<button type="submit" class="btn btn-primary" name="player"
									value="24611">William Porterfield</button>
								<br> <br>
								<button type="submit" class="btn btn-danger" name="player"
									value="303423">Andy Balbirnie</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="524845">Peter Chase</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="348034">George Dockrell</button>
								<br> <br>
								<button type="submit" class="btn btn-warning" name="player"
									value="24249">Ed Joyce</button>
								<br> <br>
								<button type="submit" class="btn btn-danger" name="player"
									value="417381">Andy McBrine</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="348059">Barry McCarthy</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="17134">Tim Murtagh</button>
								<br> <br>

							</div>
							<div class="col-sm-6">
								<button type="submit" class="btn btn-primary" name="player"
									value="24605">Kevin O'Brien</button>
								<br> <br>
								<button type="submit" class="btn btn-danger" name="player"
									value="24289">Niall O'Brien</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="303427">Paul Stirling</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="414821">Stuart Thompson</button>
								<br> <br>
								<button type="submit" class="btn btn-warning" name="player"
									value="24609">Gary Wilson</button>
								<br> <br>
								<button type="submit" class="btn btn-danger" name="player"
									value="364343">Craig Young</button>
								<br> <br>




							</div>
						</form>



						<div class="col-sm-3 space_all"></div>

					</div>
				</div>
			</div>
		</div>
	</div>
	</section>



	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">

				<p class="p_5">
					� 2018 CricWorld | All Rights Reserved | Design by SSSBM</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">�CricWorld</p>
			</div>
		</div>
	</div>
	</section>





	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>

</body>

</html>