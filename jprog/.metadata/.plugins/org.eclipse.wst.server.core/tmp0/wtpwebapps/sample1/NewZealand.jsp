<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cricworld</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>


	<!-- header begins-->

	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="home.html"><span>CRIC</span>world</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1  " href="home.html">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming
								Matches</a></li>
						<li class="dropdown"><a class="pad_1 active_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
						<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="legendary_moments.jsp">Legendary
								Moments</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>
					</li>
					</ul>
					</li>

					</ul>
					<ul class="nav navbar-nav navbar-right nav_2">



						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>

	<!-- header ends-->

	<section id="pages">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="pages">
					<div class="container" role="main">


						<h2>
							New Zealand</a>
						</h2>
						<br> <img src="img/Newzealand.jpg" width="100%"></a>
						<p>The New Zealand national cricket team, nicknamed the Black
							Caps, played their first Test in 1930 against England in
							Christchurch, becoming the fifth country to play Test cricket.
							From 1930 New Zealand had to wait until 1956, more than 26 years,
							for its first test victory, against the West Indies at Eden Park
							in Auckland. They played their first ODI in the 1972–73 season
							against Pakistan in Christchurch. The current Test, One-day and
							Twenty20 captain is Kane Williamson, who replaced Brendon
							McCullum who announced his retirement in late December, 2015. The
							national team is organised by New Zealand Cricket. The New
							Zealand cricket team became known as the Black Caps in January
							1998, after its sponsor at the time, Clear Communications, held a
							competition to choose a name for the team. Official New Zealand
							Cricket sources typeset the nickname as BLACKCAPS. This is one of
							many national team nicknames related to the All Blacks. As of 3
							April 2018, New Zealand have played 426 Test matches, winning 92,
							losing 170 and drawing 164. The team is ranked 3rd in Tests, 4th
							in ODIs and 4th in T20Is by the ICC. New Zealand reached the
							final match in the ICC Cricket World Cup for the first time in
							its history, after beating South Africa in the semi-final in
							2015.</p>


						<h2>See Also</h2>
						<form action="players.html">
							<div class="col-sm-6">
								<button type="submit" class="btn btn-primary" name="player"
									value="277906">Kane Williamson</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="277662">Corey Anderson</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="277912">Trent Boult</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="36518">Neil Broom</button>
								<br> <br>
								<button type="submit" class="btn btn-warning" name="player"
									value="55395">Colin de Grandhomme</button>
								<br> <br>
								<button type="submit" class="btn btn-danger" name="player"
									value="226492">Martin Guptill</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="319439">Mitchell McClenaghan</button>
								<br> <br>
								<button type="submit" class="btn btn-warning" name="player"
									value="388802">Tom Latham</button>
								<br> <br>

							</div>
							<div class="col-sm-6">
								<button type="submit" class="btn btn-primary" name="player"
									value="450860">Adam Milne</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="355269">James Neesham</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="38141">Jeetan Patel</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="7502">Luke Ronchi</button>
								<br> <br>
								<button type="submit" class="btn btn-warning" name="player"
									value="502714">Mitchell Santner</button>
								<br> <br>
								<button type="submit" class="btn btn-danger" name="player"
									value="232364">Tim Southee</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="38699">Ross Taylor</button>
								<br> <br>

							</div>
						</form>
						<div class="col-sm-3 space_all"></div>

					</div>
				</div>
			</div>
		</div>
	</div>
	</section>



	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">

				<p class="p_5">
					© 2018 CricWorld | All Rights Reserved | Design by SSSBM</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">©CricWorld</p>
			</div>
		</div>
	</div>
	</section>

	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>

</body>

</html>