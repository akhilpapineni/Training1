<%@page import="pack1.Null"%>
<%@page import="javax.json.JsonObject"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CricWorld</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8" async=""
	src="//s7.addthis.com/static/128.c158346ee6f2d409e386.js"></script>
</head>
<body>

	<!-- header begins-->

	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<a class="navbar-brand" href="index.html"><span>CRIC</span>World</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1  active_1" href="index.jsp">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming Matches</a></li>
						<li class="dropdown"><a class="pad_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
						<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="legendary_moments.jsp">Legendary
								Moments</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>

					<!-- Right Navigation Search-->
					<ul class="nav navbar-nav navbar-right nav_2">
						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.html">Register</a>
									</div>
								</div>
							</div>
						</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>





	</section>



	<style>
.playercard h2 {
	color: #0054df;
	font-weight: bold;
}

.playercard table {
	background: #fff;
}
</style>

	<%
		JsonObject obj = (JsonObject) request.getAttribute("object");
	%>
	<div class="row">
		<div
			style="background: url(//cdx.cricapi.com/images/banner.jpg); border-radius: 10px; min-height: 450px; margin-bottom: 50px;"
			class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12 playercard"
			itemscope="" itemtype="http://schema.org/Person">
			<center>

				<img src=<%=Null.check(obj.getJsonString("imageURL").toString())%>
					alt=<%=Null.check(obj.getJsonString("fullName").toString())%>
					title=<%=Null.check(obj.getJsonString("fullName").toString())%>
					class="img-thumbnail img-rounded" itemprop="image">

				<h2 itemprop="name">

					<img
						src="http://cdx.cricapi.com/country/Flag%20Of%20<%=obj.getJsonString("country")%>.png"
						alt=<%=obj.getJsonString("country")%>
						title=<%=obj.getJsonString("country")%> height="48"
						style="margin-right: 5px;">
					<%=Null.check(obj.getJsonString("fullName").toString())%>
				</h2>
			</center>
			<table class="table table-striped">

				<tbody>

					<tr>
						<th>Plays for</th>
						<td colspan="2" itemprop="nationality"><%=Null.check(obj.getJsonString("country").toString())%></td>
					</tr>

					<tr>
						<th>Born</th>
						<td colspan="2"><%=obj.getJsonString("born")%></td>
					</tr>

					<tr>
						<th>Batting Style</th>
						<td colspan="2"><%=obj.getJsonString("battingStyle")%></td>
					</tr>

					<tr>
						<th>Bowling Style</th>
						<td colspan="2"><%=obj.getJsonString("bowlingStyle")%></td>
					</tr>

					<tr>
						<td colspan="2"><%=obj.getJsonString("profile")%></td>
					</tr>


				</tbody>
			</table>


			<table class="table table-striped">
				<tbody>
					<tr>
						<th colspan="3">Major Teams Affiliated With</th>
					</tr>
					<tr>

						<td><%=obj.getJsonString("majorTeams")%></td>
					</tr>
				</tbody>
			</table>

			<h3>Batting Performance - as in the API</h3>
			<table class="table table-striped">
				<tbody>
					<tr>
						<th>League</th>

						<th>50</th>

						<th>100</th>

						<th>St</th>

						<th>Ct</th>

						<th>6s</th>

						<th>4s</th>

						<th>Ave</th>

						<th>HS</th>

						<th>Runs</th>

						<th>NO</th>

						<th>Inns</th>

						<th>Mat</th>
					</tr>
					<%
						String th1 = "List A", th2 = "First Class", th3 = "T20Is", th4 = "ODIs", th5 = "Tests", th = "";
						String td1 = "listA", td2 = "firstClass", td3 = "T20Is", td4 = "ODIs", td5 = "tests", td = "";
					%>

					<%
						for (int i = 1; i <= 5; i++) {
							if (i == 1) {
								td = td1;
								th = th1;
							} else if (i == 2) {
								td = td2;
								th = th2;
							} else if (i == 3) {
								td = td3;
								th = th3;
							} else if (i == 4) {
								td = td4;
								th = th4;
							} else if (i == 5) {
								td = td5;
								th = th5;
							}
					%>


					<tr>
						<th><%=th%></th>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("50")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("100")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("St")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("Ct")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("6s")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("4s")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("Ave")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("HS")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("Runs")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("NO")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("Inns")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("batting").getJsonObject(td).getJsonString("Mat")%></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>

			<h3>Bowling Performance - as in the API</h3>
			<table class="table table-striped">
				<tbody>
					<tr>
						<th>League</th>

						<th>10</th>

						<th>5w</th>

						<th>4w</th>

						<th>Econ</th>

						<th>Ave</th>

						<th>BBM</th>

						<th>BBI</th>

						<th>Wkts</th>

						<th>Runs</th>

						<th>Balls</th>

						<th>Inns</th>

						<th>Mat</th>

					</tr>

					</tr>

					<%
						for (int i = 1; i <= 5; i++) {
							if (i == 1) {
								td = td1;
								th = th1;
							} else if (i == 2) {
								td = td2;
								th = th2;
							} else if (i == 3) {
								td = td3;
								th = th3;
							} else if (i == 4) {
								td = td4;
								th = th4;
							} else if (i == 5) {
								td = td5;
								th = th5;
							}
					%>


					<tr>
						<th><%=th%></th>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("10")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("5w")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("4w")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("Econ")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("Ave")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("BBM")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("BBI")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("Wkts")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("Runs")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("Balls")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("Inns")%></td>
						<td><%=obj.getJsonObject("data").getJsonObject("bowling").getJsonObject(td).getJsonString("Mat")%></td>
					</tr>
					<%
						}
					%>

				</tbody>
			</table>


		</div>
	</div>

	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">
				<p class="p_5">
					� 2018 CricWorld | All Rights Reserved | Design by SSSBM</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">�CricWorld</p>
			</div>
		</div>
	</div>
	</section>

	<!-- Scripts -->


</body>
</html>