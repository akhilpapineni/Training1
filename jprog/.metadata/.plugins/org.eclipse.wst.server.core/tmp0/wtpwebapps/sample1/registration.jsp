<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cricworld</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<style type="text/css">
input[type="password"] {
	width: 174px;
	height: 28px;
}

input[type="date"] {
	width: 174px;
	height: 28px;
}
</style>

</head>

<body>

	<!-- header begins-->

	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html"><span>CRIC</span>World</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1  active_1" href="index.jsp">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming
								Matches</a></li>
						<li class="dropdown"><a class="pad_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="legendary_moments.jsp">Legendary
								Moments</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>

					<!-- Right Navigation Search-->
					<ul class="nav navbar-nav navbar-right nav_2">
						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												<div class="input-group">
													<input type="text" class="form-control"
														placeholder="Search"> <span
														class="input-group-btn">
														<button class="btn btn-primary" type="button">Search!</button>
													</span>
												</div>
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li><a class="pad_1" data-toggle="modal"
							data-target="#login-modal" style="cursor: pointer;">Log In</a></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
						</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>

	<!-- header ends-->


	<%
		String msg = "";
		if (request.getAttribute("msg") != null) {
			msg = request.getAttribute("msg").toString();
		}
	%>

	<form action="Register.html" method="post" id="myform">
		<br>
		<%=msg%>
		<center>
			<div class="center_left_2">
				<table class="center table-striped">
					<tr>
						<td>First Name:</td>
						<td><input type="text" name="fname" required></td>
					</tr>
					<tr>
						<td>&nbsp</td>
					</tr>
					<tr>
						<td>Last Name:</td>
						<td><input type="text" name="lname"></td>
					</tr>
					<tr>
						<td>&nbsp</td>
					</tr>
					<tr>
						<td>Date of Birth:</td>
						<td><input type="Date" name="dob" required></td>
					</tr>
					<tr>
						<td>&nbsp</td>
					</tr>
					<tr>
						<td>Gender:</td>
						<td><input type="radio" name="gender" value="M">Male
							<input type="radio" name="gender" value="F">Female <input
							type="radio" name="gender" value="O">Others</td>
					</tr>
					<tr>
						<td>&nbsp</td>
					</tr>
					<tr>
						<td>Email:</td>
						<td><input type="text" name="email" required></td>
					</tr>
					<tr>
						<td>&nbsp</td>
					</tr>
					<tr>
						<td>Phone No.:</td>
						<td><input type="tel" name="tel" required></td>
					</tr>
					<tr>
						<td>&nbsp</td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type="password" name="password" id="password"
							required></td>
					</tr>
					<tr>
						<td>&nbsp</td>
					</tr>
					<tr>
						<td>Confirm Password:</td>
						<td><input type="password" name="cpassword" id="cpassword"
							onblur="confirm()" required></td>
					</tr>
					<tr>
						<td>&nbsp</td>
					</tr>
					<tr>
						<td><input type="submit" value="Register"></td>
						<td><input type="Reset" value="Reset"></td>
					</tr>


				</table>
			</div>
		</center>
	</form>


	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">

				<p class="p_5">
					� 2018 CricWorld | All Rights Reserved | Design by SSSBM</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">�CricWorld</p>
			</div>
		</div>
	</div>
	</section>

	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>
	<script>
		function confirm() {
			var a = document.getElementById("password").value;
			var b = document.getElementById("cpassword").value;
			if (a != b) {
				alert('Password Mismatch');
			}
		}
	</script>

</body>

</html>