<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cricworld</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>


	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="home.html"><span>CRIC</span>world</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1  " href="home.html">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming
								Matches</a></li>
						<li class="dropdown"><a class="pad_1 active_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
						<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="legendary_moments.jsp">Legendary
								Moments</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>
					</li>
					</ul>
					</li>

					</ul>
					<ul class="nav navbar-nav navbar-right nav_2">



						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>

	<section id="center">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pages">
						<div class="container" role="main">


							<h2>
								ENGLAND</a>
							</h2>
							<br> <img src="img/england.jpg" width="100%"></a>
							<p>The England cricket team represents England and Wales
								(and, until 1992, also Scotland) in international cricket. Since
								1 January 1997 it has been governed by the England and Wales
								Cricket Board (ECB), having been previously governed by
								Marylebone Cricket Club (MCC) from 1903 until the end of 1996.
								England, as a founding nation, is a full member of the
								International Cricket Council (ICC) with Test, One Day
								International (ODI) and Twenty20 International (T20I) status.</p>
							<p>England and Australia were the first teams to play a Test
								match (between 15–19 March 1877), and these two countries
								together with South Africa formed the Imperial Cricket
								Conference (predecessor to today's International Cricket
								Council) on 15 June 1909. England and Australia also played the
								first ODI on 5 January 1971. England's first T20I was played on
								13 June 2005, once more against Australia.</p>
							<p>As of 04 August 2018, England has played 1000 Test
								matches, winning 358 and losing 297 (with 345 draws). The team
								has won The Ashes on 32 occasions.England has played 716 ODIs,
								winning 357,[11] and its record in major ODI tournaments
								includes finishing as runners-up in three Cricket World Cups
								(1979, 1987 and 1992), and in two ICC Champions Trophys (2004
								and 2013). England has also played 104 T20Is, winning 49. They
								won the ICC World Twenty20 in 2010, and were runners-up in 2016.
							</p>
							<br>
							<h2>See Also</h2>
							<form action="players.html">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary" name="player"
										value="24598">Eoin Morgan</button>
									<br>
									<br>
									<button type="submit" class="btn btn-danger" name="player"
										value="8917">Moeen Ali</button>
									<br>
									<br>
									<button type="submit" class="btn btn-success" name="player"
										value="297433">Jonny Bairstow</button>
									<br>
									<br>
									<button type="submit" class="btn btn-info" name="player"
										value="11728">Alastair Cook</button>
									<br>
									<br>
									<button type="submit" class="btn btn-warning" name="player"
										value="8608">James Anderson</button>
									<br>
									<br>
									<button type="submit" class="btn btn-danger" name="player"
										value="10617">Stuart Broad</button>
									<br>
									<br>
									<button type="submit" class="btn btn-info" name="player"
										value="211855">Liam Dawson</button>
									<br>
									<br>
									<button type="submit" class="btn btn-success" name="player"
										value="249866">Alex Hales</button>
									<br>
									<br>

								</div>
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary" name="player"
										value="19264">Liam Plunkett</button>
									<br>
									<br>
									<button type="submit" class="btn btn-danger" name="player"
										value="244497">Adil Rashid</button>
									<br>
									<br>
									<button type="submit" class="btn btn-success" name="player"
										value="303669">Joe Root</button>
									<br>
									<br>
									<button type="submit" class="btn btn-info" name="player"
										value="12856">Andrew Flintoff</button>
									<br>
									<br>
									<button type="submit" class="btn btn-warning" name="player"
										value="311158">Ben Stokes</button>
									<br>
									<br>
									<button type="submit" class="btn btn-danger" name="player"
										value="308251">David Willey</button>
									<br>
									<br>
									<button type="submit" class="btn btn-info" name="player"
										value="247235">Chris Woakes</button>
									<br>
									<br>



								</div>
							</form>
							<div class="col-sm-3 space_all"></div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>



	<section id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-7 footer_1">

					<p class="p_5">
						© 2018 CricWorld | All Rights Reserved | Design by SSSBM</a>
					</p>
				</div>
				<div class="col-sm-5 footer_2">
					<p class="text-right">©CricWorld</p>
				</div>
			</div>
		</div>
	</section>





	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>

</body>

</html>
