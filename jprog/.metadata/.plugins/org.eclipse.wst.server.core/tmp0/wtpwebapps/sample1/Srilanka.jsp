<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cricworld</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>


	<!-- header begins-->

	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="home.html"><span>CRIC</span>world</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1  " href="home.html">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming
								Matches</a></li>
						<li class="dropdown"><a class="pad_1 active_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="legendary_moments.jsp">Legendary
								Moments</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>
					</li>
					</ul>
					</li>

					</ul>
					<ul class="nav navbar-nav navbar-right nav_2">



						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>
	<!-- header ends-->

	<section id="pages">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="pages">
					<div class="container" role="main">


							<h2>
								Sri Lanka</a>
							</h2>
							<br> <img src="img/Srilanka.jpg" width="100%"></a>
							<p>The Sri Lanka national cricket team, nicknamed The Lions,
								represents Sri Lanka in international cricket. It is a full
								member of the International Cricket Council (ICC) with Test,
								One-Day International (ODI) and T20 International status.The
								team first played international cricket (as Ceylon) in 1926–27,
								and were later awarded Test status in 1982, which made Sri Lanka
								the eighth Test cricket playing nation. The team is administered
								by Sri Lanka Cricket. The current captain of the Sri Lankan
								cricket team in Test matches is Dinesh Chandimal. Angelo Mathews
								is the captain of the team in limited over versions. Sri Lanka's
								national cricket team achieved considerable success beginning in
								the 1990s, rising from underdog status to winning the Cricket
								World Cup in 1996. Since then, the team has continued to be a
								force in international cricket. The Sri Lankan cricket team
								reached the finals of the 2007 and 2011 Cricket World Cups
								consecutively. But they ended up being runners up in both those
								occasions.The batting of Sanath Jayasuriya, Aravinda de Silva,
								Mahela Jayawardene, Kumar Sangakkara and Tillakaratne Dilshan
								backed up by the bowling of Muttiah Muralitharan, Chaminda Vaas,
								Lasith Malinga, Ajantha Mendis, Rangana Herath, among many other
								talented cricketers, has underpinned the successes of Sri Lankan
								cricket in the last two decades.</p>
							<br>
							<h2>See Also</h2>
							<form action="players.html">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary" name="player"
										value="49764">Angelo Mathews</button>
									<br> <br>
									<button type="submit" class="btn btn-danger" name="player"
										value="50747">Upul Tharanga</button>
									<br> <br>
									<button type="submit" class="btn btn-success" name="player"
										value="300628">Dinesh Chandimal</button>
									<br> <br>
									<button type="submit" class="btn btn-info" name="player"
										value="465793">Dhananjaya de Silva</button>
									<br> <br>
									<button type="submit" class="btn btn-warning" name="player"
										value="429754">Niroshan Dickwella</button>
									<br> <br>
									<button type="submit" class="btn btn-danger" name="player"
										value="324358">Nuwan Pradeep</button>
									<br> <br>
									<button type="submit" class="btn btn-info" name="player"
										value="360456">Asela Gunaratne</button>
									<br> <br>
									<button type="submit" class="btn btn-success" name="player"
										value="345821">Danushka Gunathilaka</button>
									<br> <br>

								</div>
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary" name="player"
										value="49535">Nuwan Kulasekara</button>
									<br> <br>
									<button type="submit" class="btn btn-danger" name="player"
										value="49619">Suranga Lakmal</button>
									<br> <br>
									<button type="submit" class="btn btn-success" name="player"
										value="49758">Lasith Malinga</button>
									<br> <br>
									<button type="submit" class="btn btn-info" name="player"
										value="629074">Kusal Mendis</button>
									<br> <br>
									<button type="submit" class="btn btn-warning" name="player"
										value="233514">Thisara Perera</button>
									<br> <br>
									<button type="submit" class="btn btn-danger" name="player"
										value="429738">Lakshan Sandakan</button>
									<br> <br>
									<button type="submit" class="btn btn-info" name="player"
										value="209457">Chamara Kapugedera</button>
									<br> <br>



								</div>
							</form>
						</div>
					</div>

				</div>
				<div class="col-sm-3 space_all"></div>
			</div>
	</div>
	</div>
	</div>
	</div>
	</section>



	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">

				<p class="p_5">
					© 2018 CricWorld | All Rights Reserved | Design by SSSBM</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">©CricWorld</p>
			</div>
		</div>
	</div>
	</section>

	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>

</body>

</html>