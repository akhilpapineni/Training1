<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cricworld | Rankings</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>


	<!-- header begins-->

	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="home.html"><span>CRIC</span>World</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1 " href="home.html">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming
								Matches</a></li>
						<li class="dropdown"><a class="pad_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1  active_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="legendary_moments.jsp">Legendary
								Moments</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>

					<!-- Right Navigation Search-->
					<ul class="nav navbar-nav navbar-right nav_2">
						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
						</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>

	<!-- header ends-->

	<section id="pages">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="pages">
					<div class="container" role="main">
						<h1 class="text-center typo">ICC Team Rankings For Test, ODI,
							T20</h1>
						<h3 class="text-center">ICC Test Championship</h3>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Team</th>
											<th>Ratings</th>
											<th>Points</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>India</td>
											<td>125</td>
											<td>3634</td>
										</tr>
										<tr>
											<td>2</td>
											<td>South Africa</td>
											<td>106</td>
											<td>3712</td>
										</tr>
										<tr>
											<td>3</td>
											<td>Australia</td>
											<td>106</td>
											<td>3499</td>
										</tr>
										<tr>
											<td>4</td>
											<td>New Zealand</td>
											<td>102</td>
											<td>2354</td>
										</tr>
										<tr>
											<td>5</td>
											<td>England</td>
											<td>97</td>
											<td>3772</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Sri Lanka</td>
											<td>97</td>
											<td>3668</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Pakistan</td>
											<td>88</td>
											<td>1853</td>
										</tr>
										<tr>
											<td>8</td>
											<td>West Indies</td>
											<td>77</td>
											<td>2235</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Bangladesh</td>
											<td>67</td>
											<td>1268</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Zimbawe</td>
											<td>2</td>
											<td>12</td>
										</tr>

									</tbody>
								</table>
							</div>
							<h3 class="text-center">ICC ODI Championship</h3>
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Team</th>
											<th>Ratings</th>
											<th>Points</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>England</td>
											<td>127</td>
											<td>6470</td>
										</tr>
										<tr>
											<td>2</td>
											<td>India</td>
											<td>121</td>
											<td>5819</td>
										</tr>
										<tr>
											<td>3</td>
											<td>South Africa</td>
											<td>114</td>
											<td>4221</td>
										</tr>
										<tr>
											<td>4</td>
											<td>New Zealand</td>
											<td>112</td>
											<td>4602</td>
										</tr>
										<tr>
											<td>5</td>
											<td>Paklistan</td>
											<td>104</td>
											<td>3844</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Australia</td>
											<td>100</td>
											<td>3699</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Bangladesh</td>
											<td>92</td>
											<td>2477</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Sri Lanka</td>
											<td>76</td>
											<td>3492</td>
										</tr>
										<tr>
											<td>9</td>
											<td>West Indies</td>
											<td>69</td>
											<td>2217</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Afganistan</td>
											<td>63</td>
											<td>1758</td>
										</tr>
										<tr>
											<td>11</td>
											<td>Zimbabwe</td>
											<td>53</td>
											<td>2242</td>
										</tr>
										<tr>
											<td>12</td>
											<td>Ireland</td>
											<td>38</td>
											<td>766</td>
										</tr>
										<tr>
											<td>13</td>
											<td>Scotland</td>
											<td>33</td>
											<td>535</td>
										</tr>
										<tr>
											<td>14</td>
											<td>UAE</td>
											<td>18</td>
											<td>236</td>
										</tr>


									</tbody>
								</table>
							</div>
						</div>

						<h3 class="text-center">ICC T20 Championship</h3>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Matches</th>
											<th>Points</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Pakistan</td>
											<td>132</td>
											<td>3972</td>
										</tr>
										<tr>
											<td>2</td>
											<td>India</td>
											<td>124</td>
											<td>4601</td>
										</tr>
										<tr>
											<td>3</td>
											<td>Australia</td>
											<td>122</td>
											<td>2570</td>
										</tr>
										<tr>
											<td>4</td>
											<td>England</td>
											<td>117</td>
											<td>2448</td>
										</tr>
										<tr>
											<td>5</td>
											<td>New Zealand</td>
											<td>116</td>
											<td>2542</td>
										</tr>
										<tr>
											<td>6</td>
											<td>South Africa</td>
											<td>114</td>
											<td>2058</td>
										</tr>
										<tr>
											<td>7</td>
											<td>West Indies</td>
											<td>106</td>
											<td>2219</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Afghanistan</td>
											<td>91</td>
											<td>2287</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Sri Lanka</td>
											<td>85</td>
											<td>2287</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Bangladesh</td>
											<td>77</td>
											<td>2066</td>
										</tr>
										<tr>
											<td>11</td>
											<td>Scotland</td>
											<td>62</td>
											<td>927</td>
										</tr>
										<tr>
											<td>12</td>
											<td>Zimbabwe</td>
											<td>56</td>
											<td>1006</td>
										</tr>
										<tr>
											<td>13</td>
											<td>UAE</td>
											<td>51</td>
											<td>608</td>
										</tr>
										<tr>
											<td>14</td>
											<td>Netherlands</td>
											<td>50</td>
											<td>596</td>
										</tr>
										<tr>
											<td>15</td>
											<td>Hong Kong</td>
											<td>42</td>
											<td>420</td>
										</tr>
										<tr>
											<td>16</td>
											<td>Oman</td>
											<td>39</td>
											<td>270</td>
										</tr>
										<tr>
											<td>17</td>
											<td>Ireland</td>
											<td>35</td>
											<td>589</td>
										</tr>
									</tbody>
								</table>
							</div>


							<h1 class="text-center typo">ICC Player Rankings For Test,
								ODI, T20</h1>
							<h2 class="text-center">Batsmen</h2>
							<h3>Test</h3>
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Player</th>
											<th>Country</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Virat Kohli</td>
											<td>India</td>
											<td>934</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Steven Smith</td>
											<td>Australia</td>
											<td>929</td>
										</tr>

										<tr>
											<td>3</td>
											<td>Joe Root</td>
											<td>England</td>
											<td>865</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Kane Williamson</td>
											<td>New Zealand</td>
											<td>847</td>
										</tr>
										<tr>
											<td>5</td>
											<td>David Warner</td>
											<td>Australia</td>
											<td>820</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Cheteshwara Pujara</td>
											<td>India</td>
											<td>791</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Dimuth Karunaratne</td>
											<td>Sri Lanka</td>
											<td>754</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Dinesh Chandimal</td>
											<td>Sri Lanka</td>
											<td>733</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Dean Elgar</td>
											<td>South Africa</td>
											<td>724</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Aiden Markram</td>
											<td>South Africa</td>
											<td>703</td>
										</tr>

									</tbody>
								</table>
							</div>
							<h3>ODI</h3>
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Player</th>
											<th>Country</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Virat Kohli</td>
											<td>India</td>
											<td>911</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Babar Azam</td>
											<td>Pakistan</td>
											<td>825</td>
										</tr>

										<tr>
											<td>3</td>
											<td>Joe Root</td>
											<td>England</td>
											<td>818</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Rohit Sharma</td>
											<td>India</td>
											<td>806</td>
										</tr>
										<tr>
											<td>5</td>
											<td>David Warner</td>
											<td>Australia</td>
											<td>803</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Ross Taylor</td>
											<td>New Zealand</td>
											<td>785</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Quinton de Kock</td>
											<td>South Africa</td>
											<td>783</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Faf du Plessis</td>
											<td>South africa</td>
											<td>782</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Kane Williamson</td>
											<td>New Zealand</td>
											<td>778</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Shikhar Dhawan</td>
											<td>India</td>
											<td>770</td>
										</tr>
									</tbody>
								</table>
							</div>
							<h3>T20</h3>
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Player</th>
											<th>Country</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Aaron Finch</td>
											<td>Australia</td>
											<td>891</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Fakhar Zaman</td>
											<td>Pakistan</td>
											<td>842</td>
										</tr>

										<tr>
											<td>3</td>
											<td>Lokesh Rahul</td>
											<td>India</td>
											<td>812</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Colin Munro</td>
											<td>New Zealand</td>
											<td>801</td>
										</tr>
										<tr>
											<td>5</td>
											<td>Babar Azam</td>
											<td>Pakistan</td>
											<td>765</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Glane Maxwell</td>
											<td>Australia</td>
											<td>761</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Martin Guptill</td>
											<td>New Zealand</td>
											<td>747</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Alex Hales</td>
											<td>England</td>
											<td>710</td>
										</tr>
										<tr>
											<td>9</td>
											<td>D Arcy Short</td>
											<td>Australia</td>
											<td>690</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Rohit Sharma</td>
											<td>India</td>
											<td>678</td>
										</tr>
									</tbody>
								</table>
							</div>
							<h2 class="text-center">Bowler</h2>
							<h3>Test</h3>
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Player</th>
											<th>Country</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>James Anderson</td>
											<td>England</td>
											<td>884</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Kagiso Rabada</td>
											<td>South Africa</td>
											<td>882</td>
										</tr>

										<tr>
											<td>3</td>
											<td>Ravindra Jadeja</td>
											<td>India</td>
											<td>857</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Vernon Philander</td>
											<td>South Africa</td>
											<td>826</td>
										</tr>
										<tr>
											<td>5</td>
											<td>Ravichandran Ashwin</td>
											<td>India</td>
											<td>825</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Pat Cummins</td>
											<td>Australia</td>
											<td>800</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Trent Boult</td>
											<td>New Zeland</td>
											<td>795</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Rangana Herath</td>
											<td>Sri Lanka</td>
											<td>791</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Neil Wagner</td>
											<td>New Zealand</td>
											<td>765</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Josh Hazlewood</td>
											<td>Australia</td>
											<td>759</td>
										</tr>
									</tbody>
								</table>
							</div>
							<h3>ODI</h3>
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Player</th>
											<th>Country</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Jasprit Bumrah</td>
											<td>India</td>
											<td>775</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Rashid Khan</td>
											<td>Afganistan</td>
											<td>763</td>
										</tr>

										<tr>
											<td>3</td>
											<td>Hasan Ali</td>
											<td>Pakistan</td>
											<td>742</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Trent Boult</td>
											<td>New Zealand</td>
											<td>699</td>
										</tr>
										<tr>
											<td>5</td>
											<td>Josh Hazelewood</td>
											<td>Australia</td>
											<td>696</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Kuldeep Yadav</td>
											<td>India</td>
											<td>684</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Imran Tahir</td>
											<td>South Africa</td>
											<td>795</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Adil Rashid</td>
											<td>England</td>
											<td>681</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Kagiso Rabada</td>
											<td>South Africa</td>
											<td>679</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Yuzvendra Chahal</td>
											<td>India</td>
											<td>666</td>
										</tr>
									</tbody>
								</table>
							</div>
							<h3>T20</h3>
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Player</th>
											<th>Country</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Rashid Khan</td>
											<td>Afganistan</td>
											<td>813</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Shadab Khan</td>
											<td>Pakistan</td>
											<td>723</td>
										</tr>

										<tr>
											<td>3</td>
											<td>Ish Sodhi</td>
											<td>NewZeland</td>
											<td>700</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Yuzvendra Chahal</td>
											<td>India</td>
											<td>685</td>
										</tr>
										<tr>
											<td>5</td>
											<td>Mitchell Santner</td>
											<td>NewZeland</td>
											<td>665</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Andrew Tye</td>
											<td>Australia</td>
											<td>658</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Samuel Badree</td>
											<td>WestIndies</td>
											<td>655</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Imran Tahir</td>
											<td>SouthAfrica</td>
											<td>650</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Adil Rashid</td>
											<td>England</td>
											<td>639</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Mohammad Nabi</td>
											<td>Afganistan</td>
											<td>638</td>
										</tr>
									</tbody>
								</table>
							</div>
							<h2 class="text-center">All-Rounder</h2>
							<h3>Test</h3>
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Player</th>
											<th>Country</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Shakib Al Hasan</td>
											<td>Bangladesh</td>
											<td>420</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Ravindra Jadeja</td>
											<td>India</td>
											<td>385</td>
										</tr>

										<tr>
											<td>3</td>
											<td>Vernon Philander</td>
											<td>SouthAfrica</td>
											<td>370</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Ravichandran Ashwin</td>
											<td>India</td>
											<td>359</td>
										</tr>
										<tr>
											<td>5</td>
											<td>Jason Holder</td>
											<td>WestIndies</td>
											<td>354</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Ben Stokes</td>
											<td>England</td>
											<td>341</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Moeen Ali</td>
											<td>England</td>
											<td>258</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Pat Cummins</td>
											<td>Australia</td>
											<td>248</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Mitchell Starc</td>
											<td>Australia</td>
											<td>247</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Dilruwan Perera</td>
											<td>SriLanka</td>
											<td>224</td>
										</tr>
									</tbody>
								</table>
							</div>
							<h3>ODI</h3>
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Player</th>
											<th>Country</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Shakib Al Hasan</td>
											<td>Bangladesh</td>
											<td>362</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Mohammad Nabi</td>
											<td>Afganistan</td>
											<td>323</td>
										</tr>

										<tr>
											<td>3</td>
											<td>Mohammad Hafeez</td>
											<td>Pakistan</td>
											<td>322</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Mitchell Santner</td>
											<td>NewZeland</td>
											<td>317</td>
										</tr>
										<tr>
											<td>5</td>
											<td>Angelo Mathews</td>
											<td>SriLanka</td>
											<td>306</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Moeen Ali</td>
											<td>England</td>
											<td>301</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Rashid Khan</td>
											<td>Afganistan</td>
											<td>298</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Chris Woakes</td>
											<td>England</td>
											<td>285</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Jason Holder</td>
											<td>WestIndies</td>
											<td>283</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Mitchell Marsh</td>
											<td>Australia</td>
											<td>260</td>
										</tr>
									</tbody>
								</table>
							</div>
							<h3>T20</h3>
							<div class="col-md-12">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Position</th>
											<th>Player</th>
											<th>Country</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Glenn Maxwell</td>
											<td>Australia</td>
											<td>366</td>
										</tr>
										<tr>
											<td>2</td>
											<td>Mohammad Nabi</td>
											<td>Afganistan</td>
											<td>321</td>
										</tr>

										<tr>
											<td>3</td>
											<td>Shakib Al Hasan</td>
											<td>Bangladesh</td>
											<td>310</td>
										</tr>
										<tr>
											<td>4</td>
											<td>Jean-Paul Duminy</td>
											<td>SouthAfrica</td>
											<td>235</td>
										</tr>
										<tr>
											<td>5</td>
											<td>Marlon Samuels</td>
											<td>WestIndies</td>
											<td>222</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Paul Stirling</td>
											<td>Ireland</td>
											<td>218</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Thisara Perera</td>
											<td>SriLanka</td>
											<td>214</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Mahmudullah</td>
											<td>Bangladesh</td>
											<td>213</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Richie Berrington</td>
											<td>Scotland</td>
											<td>202</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Samiullah Shenwari</td>
											<td>Afganistan</td>
											<td>199</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	</section>



	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">

				<p class="p_5">
					� 2018 CricWorld | All Rights Reserved | Design by SSSBM</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">�CricWorld</p>
			</div>
		</div>
	</div>
	</section>

	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>

</body>

</html>