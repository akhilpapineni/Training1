<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CricApp | Contact us</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>


	<!-- header begins-->

	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
					<a class="navbar-brand" href="home.html"><span>CRIC</span>APP</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1  " href="home.html">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming Matches</a></li>
						<li class="dropdown"><a class="pad_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1 active_1" href="contact.jsp">Contact</a></li>
					</ul>

					<!-- Right Navigation Search-->
					<ul class="nav navbar-nav navbar-right nav_2">
						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
						</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>

	<!-- header ends-->

	<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center">Contact Us</h1>
				<div class="col-sm-4">
					<div class="contact_1 text-center">
						<p class="text_1">
							<i class="fa fa-map-marker"></i>
						</p>
						<h3>
							<a
								href="https://www.google.com/maps/place/Nexwave+Learning+Management+Solution/@12.9785215,77.7228695,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae11f4887dd51f:0x2149a77301b49871!8m2!3d12.9785215!4d77.7250582">Address</a>
						</h3>
						<span class="text_1">Nexwave, 4th Floor, Shailendra Tech Park, Whitefield, Bengaluru
							- INDIA</span>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="contact_1 text-center">
						<p class="text_2">
							<i class="glyphicon glyphicon-earphone"></i>
						</p>
						<h3>Phone</h3>
						<span class="text_1">Telephone: +91 9976382696</span>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="contact_1 text-center">
						<p class="text_2">
							<i class="fa fa-envelope"></i>
						</p>
						<h3>E-mail</h3>
						<span class="text_1">E-mail: <a href="detail.html">CricApp@gmail.com</a></span>
					</div>
				</div>
			</div>

		</div>
	</div>
	</section>


	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">
				<p class="p_5">
					� 2018 CricApp | All Rights Reserved | Design by Deloitte</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">�CricApp</p>
			</div>
		</div>
	</div>
	</section>

	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>

</body>

</html>