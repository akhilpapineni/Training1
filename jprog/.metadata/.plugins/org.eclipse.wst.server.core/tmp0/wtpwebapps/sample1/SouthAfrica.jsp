<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cricworld</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>


	<!-- header begins-->
<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="home.html"><span>CRIC</span>world</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1  " href="home.html">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming
								Matches</a></li>
						<li class="dropdown"><a class="pad_1 active_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="legendary_moments.jsp">Legendary
								Moments</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>
					</li>
					</ul>
					</li>

					</ul>
					<ul class="nav navbar-nav navbar-right nav_2">



						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>

	<!-- header ends-->

	<section id="pages">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="pages">
					<div class="container" role="main">


						<h2>
							South Africa</a>
						</h2>
						<br> <img src="img/Southafrica.jpg" width="100%"></a>
						<p>The South African national cricket team, nicknamed the
							Proteas (after South Africa's national flower, Protea cynaroides,
							commonly known as the "king protea"), is administered by Cricket
							South Africa. South Africa is a full member of the International
							Cricket Council (ICC) with Test, One Day International (ODI) and
							Twenty20 International (T20I) status. South Africa entered
							first-class and international cricket at the same time when they
							hosted an England team in the 1888–89 season. At first, the team
							was no match for Australia or England but, having gained in
							experience and expertise, they were able to field a competitive
							team in the first decade of the 20th century. The team regularly
							played against Australia, England and New Zealand through to the
							1960s, by which time there was considerable opposition to the
							country's apartheid policy and an international ban was imposed
							by the ICC, commensurate with actions taken by other global
							sporting bodies. When the ban was imposed, South Africa had
							developed to a point where its team including Eddie Barlow,
							Graeme Pollock and Mike Procter was arguably the best in the
							world and had just outplayed Australia.</p>

						<h2>See Also</h2>
						<form action="players.html">
							<div class="col-sm-6">
								<button type="submit" class="btn btn-primary" name="player"
									value="44936">AB de Villiers</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="43906">Hashim Amla</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="44410">Farhaan Behardien</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="379143">Quinton de Kock</button>
								<br> <br>
								<button type="submit" class="btn btn-warning" name="player"
									value="44932">Jean-Paul Duminy</button>
								<br> <br>
								<button type="submit" class="btn btn-danger" name="player"
									value="44828">Faf du Plessis</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="40618">Imran Tahir</button>
								<br> <br>
								<button type="submit" class="btn btn-warning" name="player"
									value="267724">Keshav Maharaj</button>
								<br> <br>

							</div>
							<div class="col-sm-6">
								<button type="submit" class="btn btn-primary" name="player"
									value="321777">David Miller</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="46538">Morne Morkel</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="439952">Chris Morris</button>
								<br> <br>
								<button type="submit" class="btn btn-info" name="player"
									value="265564">Wayne Parnell</button>
								<br> <br>
								<button type="submit" class="btn btn-warning" name="player"
									value="540316">Andile Phehlukwayo</button>
								<br> <br>
								<button type="submit" class="btn btn-danger" name="player"
									value="327830">Dwaine Pretorius</button>
								<br> <br>
								<button type="submit" class="btn btn-success" name="player"
									value="550215">Kagiso Rabada</button>
								<br> <br>

							</div>
						</form>
					</div>
				</div>

			</div>
			<div class="col-sm-3 space_all"></div>

		</div>
	</div>
	</div>
	</div>
	</div>
	</section>



	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">

				<p class="p_5">
					© 2018 CricWorld | All Rights Reserved | Design by SSSBM</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">©CricWorld</p>
			</div>
		</div>
	</div>
	</section>

	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>

</body>

</html>