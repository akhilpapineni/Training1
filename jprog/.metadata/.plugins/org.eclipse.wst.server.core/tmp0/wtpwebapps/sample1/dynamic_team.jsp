<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cricapp</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>


	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.jsp"><span>CRIC</span>app</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1" href="index.jsp">Home</a></li>
						<li><a class="pad_1 active_1" href="#">Teams</a></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>
					</li>

					</ul>
					<ul class="nav navbar-nav navbar-right nav_2">



						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												<div class="input-group">
													<input type="text" class="form-control"
														placeholder="Search"> <span
														class="input-group-btn">
														<button class="btn btn-primary" type="button">
															Go!</button>
													</span>
												</div>
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li><a class="pad_1" data-toggle="modal"
							data-target="#login-modal" style="cursor: pointer;">Log In</a></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
						</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>

	<section id="center">
	<div class="container">
		<div class="row">
			<div class="center clearfix">
				<div class="col-sm-9">


					<div class="blog_main border_bottom_1">
						<div class="blog">


							<h2>
								INDIA</a>
							</h2>
							<br> <img src="img/India.jpg" width="100%"></a>
							<p>Vestibulum lacinia arcu eget nulla.Class aptent taciti
								sociosqu ad litora torquent per conubia nostra, per inceptos
								himenaeos. Curabitursodales ligula in libero.</p>
						</div>
					</div>

				</div>
				<div class="col-sm-3 space_all">
					<div class="center_right">

						<div class="coverage clearfix marg_1">
							<div class="coverage_inner clearfix">
								<div class="col-sm-6 space_all coverage_inner_left">
									<h5>Related News</h5>
								</div>
								<div class="col-sm-6 space_all coverage_inner_right">
									<p class="text-right">
										<a href="detail.html">All Popular News <i
											class="fa fa-chevron-right"></i></a>
									</p>
								</div>
							</div>
							<div class="coverage_inner_1 clearfix">
								<div class="col-sm-3 space_all coverage_inner_1_left">
									<a href="detail.html"></a>
								</div>
								<div class="col-sm-9  coverage_inner_1_right">
									<h6></h6>
									<p></p>
								</div>
							</div>
							<div class="coverage_inner_1 clearfix">
								<div class="col-sm-3 space_all coverage_inner_1_left">
									<a href="detail.html"></a>
								</div>
								<div class="col-sm-9  coverage_inner_1_right">
									<h6></h6>
									<p></p>
								</div>
							</div>
							<div class="coverage_inner_1 clearfix">
								<div class="col-sm-3 space_all coverage_inner_1_left">
									<a href="detail.html"><img src="img/43.jpg" width="100%"></a>
								</div>
								<div class="col-sm-9  coverage_inner_1_right">
									<h6>
										<a href="detail.html">Himenaeos Curabitursodales ligula in
											libero.</a>
									</h6>
									<p>Nulla quis sem at nibh elementum imperdiet. Duis
										sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue
										semper porta.</p>
								</div>
							</div>
							<div class="coverage_inner_1 clearfix">
								<div class="col-sm-3 space_all coverage_inner_1_left">
									<a href="detail.html"><img src="img/44.jpg" width="100%"></a>
								</div>
								<div class="col-sm-9  coverage_inner_1_right">
									<h6>
										<a href="detail.html">Torquent per conubia nostra, per
											inceptos himenaeos</a>
									</h6>
									<p>Fusce nec tellus sed augue semper porta. Mauris
										massa.Vestibulum lacinia arcu eget nulla.Class aptent taciti
										sociosqu ad litora torquent per conubia nostra, per inceptos
										himenaeos. Curabitursodales ligula in libero.</p>
								</div>
							</div>
							<div class="coverage_inner_1 clearfix border_none_1">
								<div class="col-sm-3 space_all coverage_inner_1_left">
									<a href="detail.html"><img src="img/45.jpg" width="100%"></a>
								</div>
								<div class="col-sm-9  coverage_inner_1_right">
									<h6>
										<a href="detail.html">ligula in libero sed dignissim</a>
									</h6>
									<p>Class aptent taciti sociosqu ad litora torquent per
										conubia nostra, per inceptos himenaeos. Curabitursodales
										ligula in libero. Sed dignissim lacinia nunc.</p>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>



	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">
				<p class="p_4">
					<a href="detail.html">OUR SITE</a> <a href="detail.html">CONTACT</a>
					<a href="detail.html">ABOUT OUR SITE</a> <a href="detail.html">OUR
						POLICY</a> <a class="border_none_1" href="detail.html">TERMS</a>
				</p>
				<p class="p_5">
					� 2018 CricWorld | All Rights Reserved | Design by Pandavs</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">�CricWorld</p>
			</div>
		</div>
	</div>
	</section>

	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>

</body>

</html>