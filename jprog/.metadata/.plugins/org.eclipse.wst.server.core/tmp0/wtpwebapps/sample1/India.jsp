<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cricworld</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>


	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="home.html"><span>CRIC</span>world</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1  " href="home.html">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming
								Matches</a></li>
						<li class="dropdown"><a class="pad_1 active_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
						<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="legendary_moments.jsp">Legendary
								Moments</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>
					</li>
					</ul>
					</li>

					</ul>
					<ul class="nav navbar-nav navbar-right nav_2">



						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>

	<section id="center">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="pages">
					<div class="container" role="main">


						<h2>
							INDIA</a>
						</h2>
						<br> <img src="img/india.jpg" width="100%"></a>
						<p>The India national cricket team, also known as Team India
							and Men in Blue, is governed by the Board of Control for Cricket
							in India (BCCI), and is a full member of the International
							Cricket Council (ICC) with Test, One Day International (ODI) and
							Twenty20 International (T20I) status. Although cricket was
							introduced to India by European merchant sailors in the 18th
							century, and the first cricket club was established in Calcutta
							(currently known as Kolkata) in 1792, India's national cricket
							team did not play its first Test match until 25 June 1932 at
							Lord's, becoming the sixth team to be granted Test cricket
							status. In its first fifty years of international cricket, India
							was one of the weaker teams, winning only 35 of the first 196
							Test matches it played. From 1932 India had to wait until 1952,
							almost 20 years for its first Test victory. The team, however,
							gained strength in the 1970s with the emergence of players such
							as batsmen Sunil Gavaskar and Gundappa Viswanath, all-rounder
							Kapil Dev and the Indian spin quartet of Erapalli Prasanna,
							Srinivas Venkataraghavan, Bhagwat Chandrasekhar and Bishen Singh
							Bedi.</p>
						<p>Traditionally much stronger at home than abroad, the Indian
							team has improved its overseas form, especially in limited-overs
							cricket, since the start of the 21st century, winning Test
							matches in Australia, England and South Africa. It has won the
							Cricket World Cup twice – in 1983 under the captaincy of Kapil
							Dev and in 2011 under the captaincy of Mahendra Singh Dhoni.
							After winning the 2011 World Cup, India became only the third
							team after West Indies and Australia to have won the World Cup
							more than once, and the first cricket team to win the World Cup
							at home. It also won the 2007 ICC World Twenty20 and 2013 ICC
							Champions Trophy, under the captaincy of MS Dhoni. It was also
							the joint champions of 2002 ICC Champions Trophy, along with Sri
							Lanka. As of 2 May 2018, India is ranked first in Test 2nd in
							ODIs and third in T20Is by the ICC. Virat Kohli is the current
							captain of the team across all formats, while the head coach is
							Ravi Shastri.The Indian cricket team has rivalries with other
							Test-playing nations, most notably with Pakistan, the political
							arch-rival of India. However, in recent times, rivalries with
							nations like Australia, South Africa and England have also gained
							prominence</p>
						<br>


						<h2>See Also</h2>
						<form action="players.html">
							<div class="col-sm-6">
								<button type="submit" class="btn btn-success" name="player"
									value="35320">Sachin Tendulkar</button>
								<br>
								<br>
								<button type="submit" class="btn btn-primary" name="player"
									value="28081">MS Dhoni</button>
								<br>
								<br>
								<button type="submit" class="btn btn-danger" name="player"
									value="253802">Virat Kohli</button>
								<br>
								<br>
								<button type="submit" class="btn btn-success" name="player"
									value="26421">Ravichandran Ashwin</button>
								<br>
								<br>
								<button type="submit" class="btn btn-info" name="player"
									value="35263">Virender Sehwag</button>
								<br>
								<br>
								<button type="submit" class="btn btn-warning" name="player"
									value="28235">Shikhar Dhawan</button>
								<br>
								<br>
								<button type="submit" class="btn btn-danger" name="player"
									value="234675">Ravindra Jadeja</button>
								<br>
								<br>
								<button type="submit" class="btn btn-info" name="player"
									value="290716">Kedar Jadhav</button>
								<br>
								
								<br>

							</div>
							<div class="col-sm-6">
							
								<button type="submit" class="btn btn-success" name="player"
									value="28114">Rahul Dravid</button>
								<br>
								<br>
								<button type="submit" class="btn btn-primary" name="player"
									value="326016">Bhuvneshwar Kumar</button>
								<br>
								<br>
								<button type="submit" class="btn btn-danger" name="player"
									value="481896">Mohammed Shami</button>
								<br>
								<br>
								<button type="submit" class="btn btn-success" name="player"
									value="625371">Hardik Pandya</button>
								<br>
								<br>
								<button type="submit" class="btn btn-info" name="player"
									value="277916">Ajinkya Rahane</button>
								<br>
								<br>
								<button type="submit" class="btn btn-warning" name="player"
									value="34102">Rohit Sharma</button>
								<br>
								<br>
								<button type="submit" class="btn btn-danger" name="player"
									value="376116">Umesh Yadav</button>
								<br>
								<br>
								<button type="submit" class="btn btn-info" name="player"
									value="36084">Yuvraj Singh</button>
								<br>
								<br>



							</div>
						</form>
						<div class="col-sm-3 space_all"></div>

					</div>
				</div>
			</div>
		</div>
	</div>
	</section>



	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">

				<p class="p_5">
					© 2018 CricWorld | All Rights Reserved | Design by SSSBM</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">©CricWorld</p>
			</div>
		</div>
	</div>
	</section>

	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>

</body>

</html>
