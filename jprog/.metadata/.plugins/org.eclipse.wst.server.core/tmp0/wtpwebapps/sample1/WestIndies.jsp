<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cricworld</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
<link
	href="https://fonts.googleapis.com/css?family=Lustria|Roboto|Saira+Semi+Condensed"
	rel="stylesheet">
<script src="js/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</head>

<body>


	<!-- header begins-->

	<section id="header" class="cd-secondary-nav">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation"> <!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="home.html"><span>CRIC</span>world</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-1">

					<ul class="nav navbar-nav nav_1">
						<li><a class="pad_1  " href="home.html">Home</a></li>
						<li><a class="pad_1 " href="upcoming.html">Upcoming
								Matches</a></li>
						<li class="dropdown"><a class="pad_1 active_1" data-toggle="dropdown"
							role="button" aria-expanded="false">Teams</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="England.jsp">England</a></li>
								<li><a href="India.jsp">India</a></li>
								<li><a href="Ireland.jsp">Ireland</a></li>
								<li><a href="NewZealand.jsp">New Zealand</a></li>
								<li><a href="SouthAfrica.jsp">SouthAfrica</a></li>
								<li><a href="Srilanka.jsp">SriLanka</a></li>
								<li><a href="WestIndies.jsp">WestIndies</a></li>
						</ul></li>
						<li><a class="pad_1" href="rankings.jsp">Rankings</a></li>
						<li><a class="pad_1" href="legendary_moments.jsp">Legendary
								Moments</a></li>
						<li><a class="pad_1" href="contact.jsp">Contact</a></li>
					</ul>
					</li>
					</ul>
					</li>

					</ul>
					<ul class="nav navbar-nav navbar-right nav_2">



						<li class="dropdown"><a class="text_1 pad_1"
							href="detail.html" data-toggle="dropdown"><span
								class="glyphicon glyphicon-search"></span></a>
							<ul class="dropdown-menu drop_inner" style="min-width: 300px;">
								<li>
									<div class="row_1">
										<div class="col-md-12">
											<form class="navbar-form navbar-left" role="search">
												
											</form>
										</div>
									</div>
								</li>
							</ul></li>
						<li></li>
						<div class="modal fade" id="login-modal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
							style="display: none;">
							<div class="modal-dialog">
								<div class="loginmodal-container">
									<h1>Login to Your Account</h1>
									<br>
									<form>
										<input type="text" name="user" placeholder="Username">
										<input type="password" name="pass" placeholder="Password">
										<input type="submit" name="login"
											class="login loginmodal-submit" value="Login">
									</form>

									<div class="login-help">
										<a href="registration.jsp">Register</a>
									</div>
								</div>
							</div>
					</ul>
				</div>
				<!-- /.navbar-collapse --> </nav>
			</div>
		</div>
	</div>
	</section>

	<!-- header ends-->
	<section id="pages">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="pages">
					<div class="container" role="main">



							<h2>
								West Indies</a>
							</h2>
							<br> <img src="img/Westindies.jpg" width="100%"></a>
							<p>The West Indies cricket team, colloquially known as and
								(since June 2017) officially branded as the Windies,is a
								multi-national cricket team representing the Anglophone
								Caribbean region and administered by Cricket West Indies. A
								composite team, players are selected from a chain of 15
								Caribbean territories, most of which are English-speaking
								Caribbean, which comprise several countries and dependencies. As
								of 24 June 2018, the West Indian cricket team is ranked ninth in
								the world in Tests, ninth in ODIs and seventh in T20Is in the
								official ICC rankings. From the mid-late 1970s to the early
								1990s, the West Indies team was the strongest in the world in
								both Test and One Day International cricket. A number of
								cricketers who were considered among the best in the world have
								hailed from the West Indies: Sir Garfield Sobers, Lance Gibbs,
								Gordon Greenidge, George Headley, Brian Lara, Clive Lloyd,
								Malcolm Marshall, Sir Andy Roberts, Alvin Kallicharran, Rohan
								Kanhai, Sir Frank Worrell, Sir Clyde Walcott, Sir Everton
								Weekes, Sir Curtly Ambrose, Michael Holding, Courtney Walsh,
								Joel Garner and Sir Viv Richards have all been inducted into the
								ICC Hall of Fame. The West Indies have won the ICC Cricket World
								Cup twice, in 1975 and 1979, the ICC World Twenty20 twice, in
								2012 and 2016, the ICC Champions Trophy once, in 2004, the ICC
								Under 19 Cricket World Cup once, in 2016, and were runners-up in
								the Cricket World Cup in 1983 and Under 19 Cricket World Cup in
								2004. The West Indies were the first team to win back-to-back
								World Cups (1975 and 1979), and appeared in three consecutive
								World Cup finals (1975, 1979 and 1983). The West Indies has
								hosted the 2007 Cricket World Cup and the 2010 ICC World
								Twenty20.</p>
							<br>
							<h2>See Also</h2>
							<form action="players.html">
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary" name="player"
										value="391485">Jason Holder</button>
									<br> <br>
									<button type="submit" class="btn btn-danger" name="player"
										value="230549"> Jason Mohammed</button>
									<br> <br>
									<button type="submit" class="btn btn-success" name="player"
										value="52983">Marlon Samuels</button>
									<br> <br>
									<button type="submit" class="btn btn-info" name="player"
										value="348054">Sunil Ambris</button>
									<br> <br>
									<button type="submit" class="btn btn-warning" name="player"
										value="341593">Devendra Bishoo</button>
									<br> <br>
									<button type="submit" class="btn btn-danger" name="player"
										value="556749">Miguel Cummins</button>
									<br> <br>
									<button type="submit" class="btn btn-info" name="player"
										value="51880">Chris Gayle</button>
									<br> <br>
									<button type="submit" class="btn btn-success" name="player"
										value="443150">Kyle Hope</button>
									<br> <br>

								</div>
								<div class="col-sm-6">
									<button type="submit" class="btn btn-primary" name="player"
										value="581379">Shai Hope</button>
									<br> <br>
									<button type="submit" class="btn btn-danger" name="player"
										value="670031">Alzarri Joseph</button>
									<br> <br>
									<button type="submit" class="btn btn-success" name="player"
										value="431901">Evin Lewis</button>
									<br> <br>
									<button type="submit" class="btn btn-info" name="player"
										value="315594">Ashley Nurse</button>
									<br> <br>
									<button type="submit" class="btn btn-warning" name="player"
										value="820351">Rowman Powell</button>
									<br> <br>
									<button type="submit" class="btn btn-danger" name="player"
										value="53191">Jerome Taylor</button>
									<br> <br>
									<button type="submit" class="btn btn-info" name="player"
										value="450075">
										Kesrick Williams</button> <br> <br>
								</div>
							</form>
						</div>
					</div>

				</div>
				<div class="col-sm-3 space_all"></div>
		</div>
	</div>
	</div>
	</div>
	</div>
	</section>



	<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 footer_1">

				<p class="p_5">
					� 2018 CricWorld | All Rights Reserved | Design by SSSBM</a>
				</p>
			</div>
			<div class="col-sm-5 footer_2">
				<p class="text-right">�CricWorld</p>
			</div>
		</div>
	</div>
	</section>
	<div id="toTop" class="btn btn-info" style="display: block;">
		<span class="glyphicon glyphicon-chevron-up"></span> Top
	</div>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('body')
									.append(
											'<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
							$(window).scroll(function() {
								if ($(this).scrollTop() != 0) {
									$('#toTop').fadeIn();
								} else {
									$('#toTop').fadeOut();
								}
							});
							$('#toTop').click(function() {
								$("html, body").animate({
									scrollTop : 0
								}, 600);
								return false;
							});
						});
	</script>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$(".dropdown").hover(
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideDown(0);
										$(this).toggleClass('open');
									},
									function() {
										$('.dropdown-menu', this).not(
												'.in .dropdown-menu').stop(
												true, true).slideUp(0);
										$(this).toggleClass('open');
									});

							/*****Fixed Menu******/
							var secondaryNav = $('.cd-secondary-nav'), secondaryNavTopPosition = secondaryNav
									.offset().top;
							$(window)
									.on(
											'scroll',
											function() {
												if ($(window).scrollTop() > secondaryNavTopPosition) {
													secondaryNav
															.addClass('is-fixed');
												} else {
													secondaryNav
															.removeClass('is-fixed');
												}
											});

						});
	</script>

</body>

</html>