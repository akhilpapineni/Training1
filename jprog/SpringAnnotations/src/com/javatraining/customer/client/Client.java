package com.javatraining.customer.client;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.javatraining.customer.config.AppConfig;
import com.javatraining.customer.model.BankAccount;
import com.javatraining.customer.model.Customer;

public class Client {
	
	
	public static void main(String[] args) {
		
		
		ApplicationContext context= new AnnotationConfigApplicationContext(AppConfig.class);
		
		
		Customer customer1= context.getBean(Customer.class);
		customer1.setCustomerName("Akhil");
		customer1.setCustomerId(100);
		customer1.setCustomerAddress("Agra");
		customer1.setBillAmount(10000);
       
		BankAccount bankAcc= context.getBean(BankAccount.class);
        bankAcc.setAccountNumber("6235468");
        bankAcc.setBalance(10000);
        
        //customer1.setBankAccount(bankAcc);
        
		System.out.println(customer1);
		
		
	}
	
}
