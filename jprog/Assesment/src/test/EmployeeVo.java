package test;

public class EmployeeVo {
	
	double empid;
	String empname;
	Double annualincome;
	Double incometax;
	
	
	public EmployeeVo( double empid, String empname, Double annualincome, Double incometax)
	{
		this.empid= empid;
		this.empname = empname;
		this.annualincome= annualincome;
		this.incometax= incometax;
	}


	public double getEmpid() {
		return empid;
	}


	public void setEmpid(double d) {
		this.empid = d;
	}


	public String getEmpname() {
		return empname;
	}


	public void setEmpname(String empname) {
		this.empname = empname;
	}


	public Double getAnnualincome() {
		return annualincome;
	}


	public void setAnnualincome(Double annualincome) {
		this.annualincome = annualincome;
	}


	public Double getIncometax() {
		return incometax;
	}


	public void setIncometax(Double incometax) {
		this.incometax = incometax;
	}


	@Override
	public String toString() {
		return "EmployeeVo [empid=" + empid + ", empname=" + empname + ", annualincome=" + annualincome + ", incometax="
				+ incometax + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((annualincome == null) ? 0 : annualincome.hashCode());
		result = prime * result + ((empid == null) ? 0 : empid.hashCode());
		result = prime * result + ((empname == null) ? 0 : empname.hashCode());
		result = prime * result + ((incometax == null) ? 0 : incometax.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeVo other = (EmployeeVo) obj;
		if (annualincome == null) {
			if (other.annualincome != null)
				return false;
		} else if (!annualincome.equals(other.annualincome))
			return false;
		if (empid == null) {
			if (other.empid != null)
				return false;
		} else if (!empid.equals(other.empid))
			return false;
		if (empname == null) {
			if (other.empname != null)
				return false;
		} else if (!empname.equals(other.empname))
			return false;
		if (incometax == null) {
			if (other.incometax != null)
				return false;
		} else if (!incometax.equals(other.incometax))
			return false;
		return true;
	}
	
	

}
