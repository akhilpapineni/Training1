package test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class EmployeeMain {

	public static void main(String[] args) {
		
			Scanner sc = new Scanner(System.in);
			
			System.out.println("Enter the number of Employees");
			int n = sc.nextInt();
			
			EmployeeVo emp[] = new EmployeeVo[n];
			EmployeeBo bo =  new EmployeeBo();
			
			System.out.println("Enter Employee Details: ");
			for(int i=0; i<n; i++) {
				emp[i] = new EmployeeVo(null, null, null, null);
				
				System.out.print("ID: ");
				emp[i].setEmpid(sc.nextDouble());
				
				System.out.print("Name: ");
				emp[i].setEmpid(sc.next());
				
				System.out.print("Annual Income: ");
				emp[i].setAnnualincome(sc.nextDouble());
				
				bo.calIncomeTax(emp[i]);
			}
			
			System.out.println("\nEmployees:");
			display(emp);
			
			System.out.println("\nEmployees after sorting IT in descending order:");
			Collections.sort(Arrays.asList(emp), new Employeesort());
			display(emp);
		}
		
		public static void display(EmployeeVo[] emp) {
			for(EmployeeVo e: emp) {
				System.out.println(e);
			}
		}

	}


