package test;

public class EmployeeBo {

	public void calIncomeTax(EmployeeVo employee) {
		Double annualIncome = employee.getAnnualincome();
		Double incomeTax = (double) 0;
		
	
		if(annualIncome > 2000000) 
			incomeTax = (Double) ((annualIncome-1000000) * 0.2 + 1250000);
		else if (annualIncome > 1000000) 
			incomeTax = (Double) ((annualIncome-500000) * 0.15 + 50000);
		else if (annualIncome > 500000) 
			incomeTax = (Double) (annualIncome * 0.1);
		
		employee.setIncometax(incomeTax);		
	}

		
	}

