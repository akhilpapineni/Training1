

public class AdditionFloat
 {
   public static void main(String[] arg)
    { 
      if(arg.length !=2)
            {
                System.out.println("Mismatch in the number of arguments.Enter only two numbers");
                return;
            }
      float a = Float.parseFloat(arg[0]);
      float b = Float.parseFloat(arg[1]);
      float c = a+b;
      System.out.println(" a= " +a + " b= " +b + " sum=" +c);
    }
  }