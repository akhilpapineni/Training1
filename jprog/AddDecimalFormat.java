
import java.text.DecimalFormat;
public class AddDecimalFormat
 {
   public static void main(String[] arg)
    { 
      if(arg.length !=2)
            {
                System.out.println("Mismatch in the number of arguments.Enter only two numbers");
                return;
            }
      DecimalFormat df = new DecimalFormat("#,###,##0.00");
      float a = Float.parseFloat(arg[0]);
      float b = Float.parseFloat(arg[1]);
      float c = a+b;
      System.out.println(" a= " + df.format(a) + " b= " + df.format(b) + " sum=" +df.format(c));
    }
  }