

public class Addition
 {
   public static void main(String[] arg)
    { 
      if(arg.length !=2)
            {
                System.out.println("Mismatch in the number of arguments.Enter only two numbers");
                return;
            }
      int a = Integer.parseInt(arg[0]);
      int b = Integer.parseInt(arg[1]);
      int c = a+b;
      System.out.println(" a= " +a + " b= " +b + " sum=" +c);
    }
  }