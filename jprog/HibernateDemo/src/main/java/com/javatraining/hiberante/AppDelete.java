package com.javatraining.hiberante;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class AppDelete 
{
    public static void main( String[] args ) throws SQLException, ClassNotFoundException
    {
        Customer customer=new Customer();
        customer.setCustomerId(199);
    	Configuration configuration=new Configuration().configure();
    	
    	SessionFactory factory= configuration.buildSessionFactory();
    	Session session= factory.openSession();
    	
    	Transaction transaction=session.beginTransaction();
    	session.delete(customer);
    	
    	System.out.println(customer);
    	
    	transaction.commit();
    	System.out.println("customer deleted");
    	session.close();
    	factory.close();
    	
    }
}
