package com.javatraining.hiberante;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException, ClassNotFoundException
    {
        Customer customer=new Customer(222,"mahishmathi","Mumbai",20000);
    	//Configuration configuration=new Configuration().configure();
    	AnnotationConfiguration configuration= new AnnotationConfiguration().configure();
    	SessionFactory factory= configuration.buildSessionFactory();
    	Session session= factory.openSession();
    	
    	Transaction transaction=session.beginTransaction();
    	
    	session.save(customer);
    	
    	
    	
    	System.out.println(customer);
    	
    	transaction.commit();
    	System.out.println("details are as above");
    	session.close();
    	factory.close();
    	
    }
}
