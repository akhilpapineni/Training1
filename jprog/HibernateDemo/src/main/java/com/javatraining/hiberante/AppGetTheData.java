package com.javatraining.hiberante;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class AppGetTheData 
{
    public static void main( String[] args ) throws SQLException, ClassNotFoundException
    {
    	Customer customer= new Customer(199, "Craig", "coorg", 8888);
    	
           Configuration configuration=new Configuration().configure();
    	
    	SessionFactory factory= configuration.buildSessionFactory();
    	Session session= factory.openSession();
    	
    	Transaction transaction=session.beginTransaction();
    	session.save(customer);
    	transaction.commit();
    	System.out.println("data stored");
    	session.close();
    	factory.close();
    	
    }
}
