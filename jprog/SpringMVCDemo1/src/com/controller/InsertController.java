package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Guest;

@Controller
public class InsertController {
  
	@RequestMapping("/guest")
	public ModelAndView getA(Guest guest)
	{
		ModelAndView view= new ModelAndView();
		view.setViewName("guestDetails");
		view.addObject("message","Good morning");
		view.addObject("guestInfo",guest);
		return view;
		
	}
}
