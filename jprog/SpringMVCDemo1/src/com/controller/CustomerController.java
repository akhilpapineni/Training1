package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;
import com.javatraining.customer.service.CustomerService;
import com.model.Guest;

@Controller
public class CustomerController {
  
	@Autowired
	CustomerService customerService;
	
	
	@RequestMapping("/CustomerInfo")
	public ModelAndView getA(Customer customer)
	{
		
		customerService.insertCustomer(customer);
		ModelAndView view= new ModelAndView();
		//CustomerDAO dao=new CustomerDAOImpl();
		
		view.setViewName("customerDetails");
		view.addObject("message", customer.getCustomerName() + " your details have been successfully recorded");
		
		return view;
		
	}
}
