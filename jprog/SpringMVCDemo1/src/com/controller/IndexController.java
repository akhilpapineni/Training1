package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.javatraining.customer.model.Customer;

@Controller
public class IndexController {
	
	@RequestMapping("/customerF")
	public ModelAndView customer()
	
	{
		ModelAndView view= new ModelAndView();
		view.setViewName("customerForm");
		view.addObject("custX",new Customer());
		return view;
		
		
	}

}
