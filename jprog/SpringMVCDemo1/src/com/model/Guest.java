package com.model;

import java.io.Serializable;

public class Guest implements Serializable {

	
	private String guestName;

	public Guest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Guest(String guestName) {
		super();
		this.guestName = guestName;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	@Override
	public String toString() {
		return "Guest [guestName=" + guestName + "]";
	}
	
	
}
