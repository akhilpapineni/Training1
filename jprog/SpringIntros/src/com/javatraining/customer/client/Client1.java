package com.javatraining.customer.client;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.javatraining.customer.model.Email;

public class Client1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Resource resource=new ClassPathResource("beans1.xml");
		BeanFactory factory= new XmlBeanFactory(resource);
		
		Email email= (Email)factory.getBean("ema");
		System.out.println(email);
	}

}
