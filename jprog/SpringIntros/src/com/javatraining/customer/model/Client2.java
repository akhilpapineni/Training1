package com.javatraining.customer.model;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Client2 {
	
	public static void main(String[] args) {
		
		Resource resource=new ClassPathResource("beans2.xml");
		BeanFactory factory= new XmlBeanFactory(resource);
		
		Marks marks= (Marks)factory.getBean("mar");
		System.out.println(marks);
		
	}

}
