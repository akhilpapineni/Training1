package com.javatraining.customer.model;

public class English {
	
	public String engMarks;
	public String engGrade;
	public English() {
		super();
		// TODO Auto-generated constructor stub
	}
	public English(String engMarks, String engGrade) {
		super();
		this.engMarks = engMarks;
		this.engGrade = engGrade;
	}
	@Override
	public String toString() {
		return "English [engMarks=" + engMarks + ", engGrade=" + engGrade + "]";
	}
	public String getEngMarks() {
		return engMarks;
	}
	public void setEngMarks(String engMarks) {
		this.engMarks = engMarks;
	}
	public String getEngGrade() {
		return engGrade;
	}
	public void setEngGrade(String engGrade) {
		this.engGrade = engGrade;
	}
	

}
