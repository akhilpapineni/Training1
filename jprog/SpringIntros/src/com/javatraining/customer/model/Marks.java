package com.javatraining.customer.model;

public class Marks {
	
	private English english;
	private Hindi hindi;
	private Maths maths;
	public Marks() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Marks(English english, Hindi hindi, Maths maths) {
		super();
		this.english = english;
		this.hindi = hindi;
		this.maths = maths;
	}
	@Override
	public String toString() {
		return "Marks [english=" + english + ", hindi=" + hindi + ", maths=" + maths + "]";
	}
	public English getEnglish() {
		return english;
	}
	public void setEnglish(English english) {
		this.english = english;
	}
	public Hindi getHindi() {
		return hindi;
	}
	public void setHindi(Hindi hindi) {
		this.hindi = hindi;
	}
	public Maths getMaths() {
		return maths;
	}
	public void setMaths(Maths maths) {
		this.maths = maths;
	}
	
	
	
}
