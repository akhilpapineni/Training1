package com.javatraining.customer.model;

public class Hindi {
	
	public String hinMarks;
	public String hinGrade;
	public Hindi(String hinMarks, String hinGrade) {
		super();
		this.hinMarks = hinMarks;
		this.hinGrade = hinGrade;
	}
	public Hindi() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Hindi [hinMarks=" + hinMarks + ", hinGrade=" + hinGrade + "]";
	}
	public String getHinMarks() {
		return hinMarks;
	}
	public void setHinMarks(String hinMarks) {
		this.hinMarks = hinMarks;
	}
	public String getHinGrade() {
		return hinGrade;
	}
	public void setHinGrade(String hinGrade) {
		this.hinGrade = hinGrade;
	}

	
	
}
