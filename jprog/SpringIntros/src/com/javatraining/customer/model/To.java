package com.javatraining.customer.model;

public class To {
	public String toName;
	public String toEmail;
	
	
	@Override
	public String toString() {
		return "To [toName=" + toName + ", toEmail=" + toEmail + "]";
	}


	public To(String toName, String toEmail) {
		super();
		this.toName = toName;
		this.toEmail = toEmail;
	}


	public To() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getToName() {
		return toName;
	}


	public void setToName(String toName) {
		this.toName = toName;
	}


	public String getToEmail() {
		return toEmail;
	}


	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}
	
	

}
