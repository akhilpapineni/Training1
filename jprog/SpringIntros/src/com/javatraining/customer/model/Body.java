package com.javatraining.customer.model;

public class Body {
	
	public String body;

	public Body() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Body(String body) {
		super();
		this.body = body;
	}

	@Override
	public String toString() {
		return "Body [body=" + body + "]";
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	

}
