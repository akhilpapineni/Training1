package com.javatraining.customer.model;

public class Subject {
	
	public String caption;

	public Subject() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Subject(String caption) {
		super();
		this.caption = caption;
	}

	@Override
	public String toString() {
		return "Subject [caption=" + caption + "]";
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}
	
	

}
