package com.javatraining.customer.model;

public class Maths {
	
	public String mathMarks;
	public String mathGrade;
	public Maths(String mathMarks, String mathGrade) {
		super();
		this.mathMarks = mathMarks;
		this.mathGrade = mathGrade;
	}
	public Maths() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Maths [mathMarks=" + mathMarks + ", mathGrade=" + mathGrade + "]";
	}
	public String getMathMarks() {
		return mathMarks;
	}
	public void setMathMarks(String mathMarks) {
		this.mathMarks = mathMarks;
	}
	public String getMathGrade() {
		return mathGrade;
	}
	public void setMathGrade(String mathGrade) {
		this.mathGrade = mathGrade;
	}

	
	
	
}
