package Files;

import java.io.BufferedOutputStream;
//import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.javatraining.dbcon.Customer;

public class Directory9  {
	
	public static void main(String[] args) throws IOException {
		
		Customer customer = new Customer(100, "Piyush", "Delhi", 6500);
		ObjectOutputStream stream = 
				new ObjectOutputStream 
				(new BufferedOutputStream
						(new FileOutputStream("cust.txt")));
		
		stream.writeObject(customer);
		stream.close();
		System.out.println("Customer Data Stored");
		
	}

}
