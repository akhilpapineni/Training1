package Files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Directory8 { 
	
	public static void main(String[] args) throws IOException {
		
		String readFile= "data.txt";
		String writeFile= "newData.txt";
		
		File fileR= new File(readFile);
        File fileW= new File(writeFile);
        
        FileInputStream  readStream = new FileInputStream(fileR);
		FileOutputStream writeStream = new FileOutputStream(fileW);
		
		int i=0;
		while((i=readStream.read())!=-1) ;
		  {  writeStream.write((char)i); }
		  
           System.out.println("Copied");
           readStream.close();
           writeStream.close();
}
}
