package Files;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Directory5 {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader buf= new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("please enter name: ");
		String name = buf.readLine();
		
		System.out.println("please enter marks");
		int marks= Integer.parseInt(buf.readLine());
		
		System.out.println(name + " scored " +marks);
		
	}

}
