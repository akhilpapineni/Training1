package ExceptionDemo;

public class Demo4 {
	
	public static void main(String[] args) {
		String marks="100";
		System.out.println(marks);
		int m= Integer.parseInt(marks);
		System.out.println(m+10);
		
		//primitive--> object
		int num=10;
		Integer n=new Integer(num);
		Integer num1=num;
		System.out.println(num1);
		
		
		//object-->primitive
		Integer scores=20;  
		int i=Integer.valueOf(scores);
		int latestScores= scores;
		System.out.println("latestScores");
		
		
		
	}

}
