package ExceptionDemo;

import java.util.Scanner;

public class Demo1 {

	int num1,num2,result;
	Scanner sc= new Scanner(System.in);
	
	
	public void display() {
		System.out.println("Enter first number :");
		num1=sc.nextInt();
		
		System.out.println("Enter second number :");
		num2=sc.nextInt();
		
		try {
			result=num1/num2;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("The result is:" +result);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Demo1 d =new Demo1();
		d.display();
		System.out.println("Thanks for using my program :");

	}

}
