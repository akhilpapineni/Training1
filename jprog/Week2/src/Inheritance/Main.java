package Inheritance;


	class Vehicle {
	
	String color="Blue";
	int noOfWheels;
 void start()
	 {
		System.out.println("Vehicle starts");
	}
	

}
	
	class Car extends Vehicle {
	   String carType;
	   String color="White";
	  
	   
	   
	   public void showDetails() {
		  //String color="Red";
		  int noOfWheels=4;
		   carType="HatchBack";
				   
				   System.out.println("Car color is:" + color);
				   System.out.println("Car color in super class is:" + super.color);
		           System.out.println("Car has noOfWheels :" + noOfWheels);
		           System.out.println("Car type is:" + carType);
		           
	   }
	}
	public class Main {
		
		public static void main(String[] args) 
		{
		  Car c=new Car();
		  c.start();
		  c.showDetails();
		}
	}
		