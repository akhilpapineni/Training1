package Inheritance;

abstract class Vehicle1 {

String color="Blue";
int noOfWheels;
public abstract void start();
public abstract void stop();
}

abstract class Bike extends Vehicle1
{
  @Override
  public void start() {
	  System.out.println("Bike started");
	  
  }
  public abstract void kickstart();
}

 abstract class Pulsar extends Bike
 {
	 @Override
	 public void kickstart() {
		 }
	 
	 @Override
	 public void stop() {
	  }
	
 }

public class Main1 {
	public static void main(String[] args) 
	{  
		
		Vehicle1 vehicle=new Pulsar();
		vehicle.start();
				
	}
	
	}
			
		
			

