package threaddemos;

public class Demo2 extends Thread
   {
     public Demo2(int i) 
     {
    	super("ITPL" +i);
        
    	start();
    	
     }
     
     @Override
     public void run() {
    	 System.out.println("Run called " + Thread.currentThread().getName());
     }
     public static void main(String[] args) {
    	 for(int i = 1 ; i<=5; i++)
    	 { new Demo2(i);}
    	 
    	 Thread.currentThread().setName("ITPL");
    	 
    	 System.out.println("Main called " + Thread.currentThread().getName());     }
}
