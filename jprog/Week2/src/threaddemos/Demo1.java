package threaddemos;

public class Demo1 extends Thread  implements Runnable{
	
	Thread t1,t2;
	public Demo1()
	    {
		t1=new Thread(this);
		System.out.println(Thread.currentThread().getName());
		t1.start();
		System.out.println(Thread.currentThread().getName());
		
		t2=new Thread(this);
		System.out.println(Thread.currentThread().getName());
		t2.start();
	
	    }
	
	@Override
	public void run()
	{
		System.out.println("Run called " +Thread.currentThread().getName());
	}
	

	
	public static void main(String[] args) throws InterruptedException {
		
		Demo1 d=new Demo1();
		Thread.sleep(1000);
		System.out.println("Main called " +Thread.currentThread().getName());
	}
}
