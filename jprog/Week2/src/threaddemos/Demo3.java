package threaddemos;

public class Demo3 extends Thread {
	
	
	public static synchronized void display(String threadName, String message1, String message2)
	{ System.out.println("Message by " +threadName + " thread ");
	  System.out.println("1st message, by  " +threadName+ " is: " +message1 );
	  
	  try {
		 Thread.sleep(1000);
         }
	  catch (InterruptedException e){
		  
		  e.printStackTrace();}
	  
	  System.out.println("2nd message, by  " +threadName+ " is: " +message2);
	  } 
	
	
	  
    public Demo3(int i) 
    {
   	super("" +i);
       
   	start();
   	
    }
    
    @Override
    public void run() {
   	 display(Thread.currentThread().getName(),"Hi","Bye");
    }
    public static void main(String[] args) {
   	 for(int i = 1 ; i<=3; i++)
   	 { new Demo3(i);}
   	 
   	  }
}


