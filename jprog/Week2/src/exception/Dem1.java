package exception;

import java.util.Scanner;

public class Dem1 {
	

	int num1,num2,result;
	Scanner sc= new Scanner(System.in);
	
	public void display() {
		System.out.println("Enter first number :");
		num1=sc.nextInt();
		
		System.out.println("Enter second number :");
		num2=sc.nextInt();
		
		if(num1<0 | num2<0)
			try {
				throw new NegativeNumberException("Enter positive numbers only");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
		
		
		try {
			result=num1/num2;
		} catch (Exception e) {
			// TODO Auto-generated catchblock
			e.printStackTrace();
		}
		
		System.out.println("The result is:" +result);
		
		
	}	

}
