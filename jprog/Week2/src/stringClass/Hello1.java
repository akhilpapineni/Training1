package stringClass;

public class Hello1<Z> {
	
	public <Z extends Number> double display(Z z1, Z z2 )
	{
		return z1.doubleValue() + z2.doubleValue();
		
	}
    public static void main(String[] args)
    {
    	Hello1<Float> h= new Hello1<Float>();
    	System.out.println(h.display(21.3, 18.7) );
    	
    }
}
