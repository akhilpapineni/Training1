package stringClass;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class BinarySearch {
	
	public static void main(String[] args)
	{
		List all= new ArrayList();
		all.add("Yamini");
		all.add("Anu");
		all.add("Mohan");
		all.add("Neeti");
		int position= Collections.binarySearch(all, "Yamini");
		System.out.println(position);
		
		Collections.sort(all);
		System.out.println(position);
		int position= Collections.binarySearch(all, "Neeti");
		System.out.println(position);
		
		
	}
}
