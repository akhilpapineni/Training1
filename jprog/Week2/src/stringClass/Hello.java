package stringClass;

public class Hello<Z> {
	
	public void display(Z z )
	{
		System.out.println(z);
	}
    public static void main(String[] args)
    {
    	Hello<Boolean> hello= new Hello<Boolean>();
    	hello.display(true);
    	
    	
    	Hello<String> hello1= new Hello<String>();
    	hello1.display("Pooja");
    	
    	Hello<Integer> hello2= new Hello<Integer>();
    	hello2.display(1003286);
    	
    	Hello<Float> hello3= new Hello<Float>();
    	hello3.display(2.5565788f);
    	
    	Hello<Double> hello4= new Hello<Double>();
    	hello4.display(1748237934573894658934756.368);
    	
    }
}
