package stringClass;

public class Exercise {

	public static void main(String[] args)
	{
	
	String str = "The quick brown fox jumps over the lazy dog";
			System.out.println(str);
			
			System.out.println(str.charAt(12));
			System.out.println(str.contains("is"));
			System.out.println(str.concat(" and killed it"));
			System.out.println(str.endsWith("dogs"));
			System.out.println(str.equals("The quick brown Fox jumps over the lazy Dog"));
			//System.out.println(str.contentEquals(�THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG�));
			System.out.println(str.length());
			//System.out.println(str.matches(�The quick brown Fox jumps over the lazy Dog�));
	        System.out.println(str.replace("The", "A"));
	        System.out.println(str.split("fox"));
}
}
