package stringClass;
import java.util.*;
import java.lang.*;

public class Exercise3 {
	
	public static void main(String[] args) {
		//set doesn't encourage duplicate values
	
	    Set batchList= new HashSet();
	    
		batchList.add("kapoor");
		batchList.add("kareena");
		batchList.add("ranbeer");
		batchList.add("ranbeer");
		batchList.add("chopra");
		
		System.out.println(batchList);
		
		Iterator i= batchList.iterator();
		  while(i.hasNext())
		  {
			  System.out.println(i.next());
		  }
		  
		  Set batchList1= new LinkedHashSet();
		  
		  batchList.add("kapoor");
			batchList1.add("kareena");
			batchList1.add("ranbeer");
			batchList1.add("ranbeer");
			batchList1.add("chopra");
			
			System.out.println(batchList1);
			
			Iterator i1= batchList1.iterator();
			  while(i1.hasNext())
			  {
				  System.out.println(i1.next());
			  }
			  
			  
			  Set batchList2= new TreeSet();
			  batchList2.add("kapoor");
				batchList2.add("kareena");
				batchList2.add("ranbeer");
				batchList2.add("ranbeer");
				batchList2.add("chopra");
				
				System.out.println(batchList2);
				
				Iterator i2= batchList2.iterator();
				  while(i2.hasNext())
				  {
					  System.out.println(i2.next());
				  }
		  
		  
	}
}
