package stringClass;

import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Iterator;
public class Demo2 { 
	
	public static void main(String[] args)
	{
		Customer customer1= new Customer(1,"Mohan", "Pune", 9800);
		Customer customer2= new Customer(2,"Anu","Mumbai",800);
		Customer customer3= new Customer(3,"Zeba","Agra",1800);
		Customer customer4= new Customer(4,"Uday","Jaipur",2900);
		Customer customer5= new Customer(5, "Neeti", "Delhi",200);
		
		List<Customer> allCustomers= new ArrayList<Customer>();
		allCustomers.add(customer1);
		allCustomers.add(customer2);
		allCustomers.add(customer3);
		allCustomers.add(customer4);
		allCustomers.add(customer5);
		
		
		System.out.println(allCustomers);
		
		Collections.sort(allCustomers,new AddressComparator());
		 System.out.println("After sorting by Address Comparator--");
		 System.out.println(allCustomers );
		 
		 Iterator i= allCustomers.iterator();
		  while(i.hasNext())
		  {
			  System.out.println(i.next());
		  }
		 
		 Collections.sort(allCustomers,new BillAmountComparator());
		 System.out.println("After sorting by BillAmount Comparator--");
		 System.out.println(allCustomers);
		 
		
		Iterator i1= allCustomers.iterator();
		  while(i1.hasNext())
		  {
			  System.out.println(i1.next());
		  }
			  
			Collections.sort(allCustomers, new Comparator<Customer>() {

				@Override
				public int compare(Customer o1, Customer o2) {
					if(o1.getCustomerId()<o2.getCustomerId())
						return 1;
					else
					// TODO Auto-generated method stub
					return -1;
					
				}
				
			});
			System.out.println("After sorting by idComparator--");
			 System.out.println(allCustomers);

				Iterator i2= allCustomers.iterator();
				  while(i2.hasNext())
				  {
					  System.out.println(i2.next());
				  }
				
		  }
	
}
	

