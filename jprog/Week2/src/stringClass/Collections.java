package stringClass;
import java.util.ArrayList;
import java.util.Collection;
//import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Collections {
	
	public static void main(String[] args)
	{
		Customer customer1= new Customer(1,"Mohan", "Pune", 9800);
		Customer customer2= new Customer(2,"Anu","Mumbai",800);
		Customer customer3= new Customer(3,"Zeba","Agra",1800);
		Customer customer4= new Customer(4,"Uday","Jaipur",2900);
		Customer customer5= new Customer(5, "Neeti", "Delhi",200);
		
		List<Customer> allCustomers= new ArrayList<Customer>();
		allCustomers.add(customer1);
		allCustomers.add(customer2);
		allCustomers.add(customer3);
		allCustomers.add(customer4);
		allCustomers.add(customer5);
		
		
	
		 
		 Iterator i= allCustomers.iterator();
		  while(i.hasNext())
		  {
			  System.out.println(i.next());
		  }

}
